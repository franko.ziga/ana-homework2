import numpy as np
import pandas as pd
from time import time
from utils import read_test_sample
from gen import create_specimen, evaluate_specimens, reproduce

# by need uncomment imports below
# from exh import exh
# from greedy import greedy
# from fptas import fptas

DATA_FILE_PATH = "./data/s1.txt"

def dyn(numbers, k):
  numbers = sorted(numbers)
  n = len(numbers)
  matrix = np.zeros((n+1, k+1))
  # print(matrix.shape)

  for i in range(1, n+1):
    for j in range(1, k+1):
      index = j-numbers[i-1]
      if index < 0:
        matrix[i, j] = matrix[i-1, j]
      else:
        matrix[i, j] = max(
          matrix[i-1, j],
          matrix[i-1, index]+numbers[i-1]
        )
  return matrix


def dyn_reward(numbers, k):
  sd = time()
  _ = dyn(numbers, k)
  ed = time()
  _ = exh(numbers, k)
  _ = greedy(numbers, k)
  _ = fptas(numbers, k, 0.1)
  et = time()

  return (ed - sd) / ((et - ed) / 3)

dna_range = {
  "n": 1000,
  "k": 1000,
  "length": 100
}

def random_test():
  random_number = np.random.randint(0, 1000)
  random_k = np.random.randint(900, 1000)

  numbers = [random_number]
  return dyn_reward(numbers, random_k)

def random():
  numbets = np.arange(816, 820)
  numbets = np.random.choice(numbets, size=31)
  k = 740

  return dyn_reward(numbets, k)

# HAND BUILD FUNCTION
if __name__ == '__main__':
  difference = np.mean([random() for _ in range(100)])
  print(difference)

# TESTING
# if __name__ == '__main__':
#   n, k, subset = read_test_sample(DATA_FILE_PATH)
#   print(n)
#   print(k)
#   print(subset)
#   np.set_printoptions(suppress=True)
#   matrix = dyn(subset, n, k)
#   print(matrix)

# GENETIC FINDER
# if __name__ == '__main__':
#   specimens = [create_specimen(dna_range, dyn_reward) for _ in range(30)]
#   data = {
#     "Generation": [],
#     "Min result": [],
#     "Max result": [],
#     "Start n": [],
#     "End n": [],
#     "Length": [],
#     "K": [],
#   }
#   for epoch in range(100):
#     results, max_res, min_res = evaluate_specimens(specimens)
#     best = specimens[np.argmax(results)]
#     data["Generation"].append(epoch)
#     data["Max result"].append(max_res)
#     data["Min result"].append(min_res)
#     data["Start n"].append(best.start_n)
#     data["End n"].append(best.end_n)
#     data["Length"].append(best.length)
#     data["K"].append(best.k)
#     print("Epoch: {}\tMin result: {:.3f}\tMax result: {:.3f}\tStart N: {}\tEnd N:{}\tLength: {}\tK: {}".format(epoch,
#                                                                                                                min_res,
#                                                                                                                max_res,
#                                                                                                                best.start_n,
#                                                                                                                best.end_n,
#                                                                                                                best.length,
#                                                                                                                best.k))
#     specimens = reproduce(specimens, results, dna_range)
# 
#   data = pd.DataFrame(data)
#   data.set_index("Generation", inplace=True)
#   print(data)
#   data.to_csv("dyn.csv")