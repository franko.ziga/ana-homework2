import numpy as np

class DNK:
  def __init__(self, start_n, end_n, length, k, eval_fun):
    self.start_n = start_n
    self.end_n = end_n

    self.length = length

    self.k = k

    self.eval_fun = eval_fun
    self.result = -1

  def mutate(self, dna_range, rate=0.1):
    sr = rate / 4
    section = np.random.choice([0, 1, 2, 3, 4], p=[1.-rate, sr, sr, sr, sr])
    if section == 1:
      self.start_n = int(self.start_n * (np.random.rand() - .5))
      self.start_n = max(0, min(self.start_n, self.end_n-1))
    elif section == 2:
      self.end_n = int(self.end_n * (np.random.rand() - .5))
      self.end_n = max(self.start_n+1, min(self.end_n, dna_range["n"]))
    elif section == 3:
      self.length = int(self.length * (np.random.rand() - .5))
      self.length = max(1, min(self.length, dna_range["length"]))
    elif section == 4:
      self.k = int(self.k * (np.random.rand() - .5))
      self.k = max(1, min(self.k, dna_range["k"]))

    # if self.start_n >= self.end_n:
    #   print("Mutate error")

  def crossover(self, other):
    start_n = int((self.start_n + other.start_n)/2)
    end_n = int((self.end_n + other.end_n)/2)
    length = int((self.length + other.length)/2)
    k = int((self.k + other.k)/2)


    return DNK(start_n, end_n, length, k, self.eval_fun)

  def evaluate(self):
    numbers = np.arange(self.start_n, self.end_n)
    numbers = np.random.choice(numbers, size=self.length)

    self.result = np.mean([
      self.eval_fun(numbers, self.k)
      for _ in range(25)
    ])
    return self.result


def create_specimen(dna_range, eval_fun):
  range_n = dna_range["n"]
  start_n = np.random.randint(0, range_n)
  end_n = np.random.randint(start_n, range_n)
  length = np.random.randint(1, dna_range["length"])
  k = np.random.randint(1, dna_range["k"])

  return DNK(start_n, end_n, length, k, eval_fun)


def evaluate_specimens(specimens):
  results = []
  for specimen in specimens:
    results.append(specimen.evaluate())

  results = np.array(results)
  min_result = np.min(results)
  max_result = np.max(results)
  results = (results - min_result) / (max_result - min_result)
  return results, max_result, min_result


def reproduce(specimens, results, dna_range, keep=.2):
  n = len(specimens)
  new_specimens = list()
  results = results / np.sum(results)

  for _ in range(int(n * (1 - keep))):
    s1 = np.random.choice(specimens, p=results)
    s2 = np.random.choice(specimens, p=results)

    s1 = s1.crossover(s2)
    s1.mutate(dna_range)
    new_specimens.append(s1)

  results = [(i, r) for i, r in enumerate(results)]
  results = sorted(results, key=lambda x: -x[1])

  for index, _ in results:
    if n == len(new_specimens): break
    new_specimens.append(specimens[index])

  return new_specimens