import numpy as np
import pandas as pd
from time import time
from utils import read_test_sample
from gen import create_specimen, evaluate_specimens, reproduce
#
from exh import exh

def greedy(subset, k):
  subset = sorted(subset, key=lambda x: -x)
  g = 0
  result = []

  for element in subset:
    if element <= k - g:
      result.append(element)
      g += element

  return result

def greedy_reward(numbers, k):
  re = exh(numbers, k)[-1][-1]
  rg = greedy(numbers, k)
  return re - sum(rg)

def random_test():
  m = np.random.randint(1, 1000)
  numbers = [m+1, m, m]
  k = m + m
  return greedy_reward(numbers, k)


def random():
  numbers = np.arange(335, 461)
  numbers = np.random.choice(numbers, size=29)
  k = 787
  return greedy_reward(numbers, k)

def random1():
  numbers = [np.random.randint(1, 1000)] * np.random.randint(1, 100)
  k = np.random.randint(1, 10)
  return greedy_reward(numbers, k)

# TEST
# if __name__ == '__main__':
#   n, k, subset = read_test_sample("./data/s1.txt")
#   subset = sorted(subset, key=lambda x: -x)
#
#   subset = [6, 5, 5]
#   k = 10
#   result = greedy(subset, k)
#   # print(result)
#   print(k)
#   print(sum(result))

# HAND EXAMPLE TESTER
if __name__ == '__main__':
  res = np.mean([random() for _ in range(100)])
  print(res)
  res = np.mean([random_test() for _ in range(100)])
  print(res)
  res = np.mean([random1() for _ in range(100)])
  print(res)

# GENETIC FINDER
# if __name__ == '__main__':
#
#   dna_range = {
#     "n": 1000,
#     "k": 1000,
#     "length": 100
#   }
#
#   specimens = [create_specimen(dna_range, greedy_reward) for _ in range(30)]
#   data = {
#     "Generation": [],
#     "Min result": [],
#     "Max result": [],
#     "Start n": [],
#     "End n": [],
#     "Length": [],
#     "K": [],
#   }
#   for epoch in range(1000):
#     results, max_res, min_res = evaluate_specimens(specimens)
#     best = specimens[np.argmax(results)]
#     data["Generation"].append(epoch)
#     data["Max result"].append(max_res)
#     data["Min result"].append(min_res)
#     data["Start n"].append(best.start_n)
#     data["End n"].append(best.end_n)
#     data["Length"].append(best.length)
#     data["K"].append(best.k)
#     print("Epoch: {}\tMin result: {:.3f}\tMax result: {:.3f}\tStart N: {}\tEnd N:{}\tLength: {}\tK: {}".format(epoch,
#                                                                                                                min_res,
#                                                                                                                max_res,
#                                                                                                                best.start_n,
#                                                                                                                best.end_n,
#                                                                                                                best.length,
#                                                                                                                best.k))
#     specimens = reproduce(specimens, results, dna_range)
#
#   data = pd.DataFrame(data)
#   data.set_index("Generation", inplace=True)
#   print(data)
#   data.to_csv("greedy.csv")