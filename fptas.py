import numpy as np
import pandas as pd
from utils import read_test_sample
from gen import create_specimen, evaluate_specimens, reproduce

# from dyn import dyn
from exh import exh

def trim(subset, epsilon):
  result = [subset[0]]
  last = subset[0]

  for index in range(1, len(subset)):
    if subset[index] > last + last * epsilon:
      last = subset[index]
      result += [last]

  return np.array(result)

def fptas(numbers, k, epsilon):
  layers = [np.array(0)]
  n = len(numbers)
  delta = epsilon / (2 * n)

  
  for num in numbers:
    base = layers[-1]
    l = base + num
    l = np.unique(np.append(base, l))
    l = np.sort(l)
    l = trim(l, delta)
    l = l[l <= k]
    layers.append(l)

  return layers

def fptas_reward(numbers, k, epsilon=.1):
  re = exh(numbers, k)[-1][-1]
  rg = fptas(numbers, k, epsilon)[-1][-1]
  return re - rg

def random_test(epsilon=.1):
  numbers = np.arange(40, 327)
  numbers = np.random.choice(numbers, size=11)
  k = 867

  return fptas_reward(numbers, k, epsilon=epsilon)

# TEST EXAMPLE
# if __name__ == '__main__':
#   n, k, subset = read_test_sample("./data/s1.txt")
#   epsilon = .1
#   subset = [10, 11, 12, 13, 14, 15, 17, 20, 21, 22, 23, 24, 29]
#
#   print(trim(subset, epsilon))

# HAND TEST
if __name__ == '__main__':
  res = np.mean([random_test() for _ in range(100)])
  print(res)

# GENETIC FINDER
# if __name__ == '__main__':
# 
#   dna_range = {
#     "n": 1000,
#     "k": 1000,
#     "length": 100
#   }
# 
#   specimens = [create_specimen(dna_range, greedy_reward) for _ in range(30)]
#   data = {
#     "Generation": [],
#     "Min result": [],
#     "Max result": [],
#     "Start n": [],
#     "End n": [],
#     "Length": [],
#     "K": [],
#   }
#   for epoch in range(100):
#     results, max_res, min_res = evaluate_specimens(specimens)
#     best = specimens[np.argmax(results)]
#     data["Generation"].append(epoch)
#     data["Max result"].append(max_res)
#     data["Min result"].append(min_res)
#     data["Start n"].append(best.start_n)
#     data["End n"].append(best.end_n)
#     data["Length"].append(best.length)
#     data["K"].append(best.k)
#     print("Epoch: {}\tMin result: {:.3f}\tMax result: {:.3f}\tStart N: {}\tEnd N:{}\tLength: {}\tK: {}".format(epoch,
#                                                                                                                min_res,
#                                                                                                                max_res,
#                                                                                                                best.start_n,
#                                                                                                                best.end_n,
#                                                                                                                best.length,
#                                                                                                                best.k))
#     specimens = reproduce(specimens, results, dna_range)
# 
#   data = pd.DataFrame(data)
#   data.set_index("Generation", inplace=True)
#   print(data)
#   data.to_csv("fptas.csv")