import os
import numpy as np

def read_test_sample(file_path):
  with open(file_path, "r") as file:
    text = file.read()
    numbers = [int(n) for n in text.split("\n")]
    n, k = numbers[0], numbers[1]
    file.close()
    return n, k, numbers[2:]# sorted(numbers[2:])

def tester1():
  n = np.arange(10000)
  n = np.random.choice(n, size=1000)
  k = np.random.randint(1, 1000)
  return n, k
