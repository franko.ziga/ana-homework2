from dyn import dyn
from exh import exh
from greedy import greedy
from fptas import fptas
import numpy as np

from time import time

class DNK:
  def __init__(self, start_n, end_n, length, k, eval_fun):
    self.start_n = start_n
    self.end_n = end_n

    self.length = length

    self.k = k



    self.eval_fun = eval_fun
    self.result = -1

  def mutate(self, dna_range, rate=0.1):
    sr = rate / 4
    section = np.random.choice([0, 1, 2, 3, 4], p=[1.-rate, sr, sr, sr, sr])
    if section == 1: self.start_n = np.random.randint(0, self.end_n)
    elif section == 2: self.end_n = np.random.randint(self.start_n+1, dna_range["n"]+1)
    elif section == 3: self.length = np.random.randint(1, dna_range["length"])
    elif section == 4: self.k = np.random.randint(1, dna_range["k"])

  def crossover(self, other):
    start_n = int((self.start_n + other.start_n)/2)
    end_n = int((self.end_n + other.end_n)/2)
    # if end_n <= start_n:
    #   end_n = self.end_n if self.end_n > start_n else other.end_n
    length = int((self.length + other.length)/2)
    k = int((self.k + other.k)/2)
    if start_n >= end_n:
      print("Crossover error")
      print(start_n)
      print(end_n)
    return DNK(start_n, end_n, length, k, self.eval_fun)

  def evaluate(self):
    numbers = np.arange(self.start_n, self.end_n)
    numbers = np.random.choice(numbers, size=self.length)

    self.result = np.mean([
      self.eval_fun(numbers, self.k)
      for _ in range(10)
    ])
    return self.result


def create_specimen(dna_range, eval_fun):
  range_n = dna_range["n"]
  start_n = np.random.randint(0, range_n)
  end_n = np.random.randint(start_n, range_n)
  length = np.random.randint(1, dna_range["length"])
  k = np.random.randint(1, dna_range["k"])

  return DNK(start_n, end_n, length, k, eval_fun)


def test(numbers, k, epsilon=.1):
  d = dyn(numbers, k)
  e = exh(numbers, k)
  g = greedy(numbers, k)
  f = fptas(numbers, k, epsilon)
  return d, e, g, f



def dyn_reward(numbers, k):
  sd = time()
  _ = dyn(numbers, k)
  ed = time()
  _ = exh(numbers, k)
  _ = greedy(numbers, k)
  _ = fptas(numbers, k, 0.1)
  et = time()

  return (ed - sd) - (et - ed) / 3

def evaluate_specimens(specimens):
  results = []
  for specimen in specimens:
    results.append(specimen.evaluate())

  results = np.array(results)
  min_result = np.min(results)
  max_result = np.max(results)
  results = (results - min_result) / (max_result - min_result)
  return results, max_result, min_result


def reproduce(specimens, results, dna_range):
  n = len(specimens)
  new_specimens = list()
  results = results / np.sum(results)
  for _ in range(n//2):
    s1 = np.random.choice(specimens, p=results)
    s2 = np.random.choice(specimens, p=results)

    s1 = s1.crossover(s2)
    s1.mutate(dna_range)
    new_specimens.append(s1)

  results = [(i, r) for i, r in enumerate(results)]
  results = sorted(results, key=lambda x: -x[1])

  for index, _ in results[:n//2]:
    new_specimens.append(specimens[index])

  return new_specimens


dna_range = {
  "n": 10000,
  "k": 10000,
  "length": 100
}

if __name__ == '__main__':
  specimens = [create_specimen(dna_range, dyn_reward) for _ in range(30)]

  for epoch in range(100):
    results, max_res, min_res = evaluate_specimens(specimens)
    best = specimens[np.argmax(results)]
    print("Epoch: {}\tMin result: {:.3f}\tMax result: {:.3f}\tStart N: {}\tEnd N:{}\tLength: {}\tK: {}".format(epoch, min_res, max_res, best.start_n, best.end_n, best.length, best.k))
    specimens = reproduce(specimens, results, dna_range)

