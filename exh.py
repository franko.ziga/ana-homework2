import numpy as np
import pandas as pd
from time import time
from utils import read_test_sample
from gen import create_specimen, evaluate_specimens, reproduce

# from dyn import dyn
# from greedy import greedy
# from fptas import fptas

def exh(numbers, k):
  numbers = sorted(numbers)
  layers = [np.array(0)]

  for num in numbers:
    base = layers[-1]
    l = base + num

    l = np.unique(np.append(base, l))
    l = l[l <= k]
    layers.append(l)

  return layers


def exh_reward(numbers, k):
  sd = time()
  _ = exh(numbers, k)
  ed = time()
  _ = dyn(numbers, k)
  _ = greedy(numbers, k)
  _ = fptas(numbers, k, 0.1)
  et = time()

  return (ed - sd) / ((et - ed) / 3)


def random():
  numbers = np.arange(44, 255)
  numbers = np.random.choice(numbers, size=1)
  k = 1
  return exh_reward(numbers, k)

def random_test():
  numbers = [np.random.randint(1, 1000)] * np.random.randint(1, 1000)
  k = np.random.randint(1, 10)

  return exh_reward(numbers, k)

# HAND TESTING
if __name__ == '__main__':
  res = np.mean([random_test() for _ in range(100)])
  print(res)
  res = np.mean([random() for _ in range(100)])
  print(res)

# GENETIC EVOLUTION
# if __name__ == '__main__':
#   dna_range = {
#     "n": 1000,
#     "k": 1000,
#     "length": 100
#   }
#
#   specimens = [create_specimen(dna_range, exh_reward) for _ in range(30)]
#   data = {
#     "Generation": [],
#     "Min result": [],
#     "Max result": [],
#     "Start n": [],
#     "End n": [],
#     "Length": [],
#     "K": [],
#   }
#   for epoch in range(100):
#     results, max_res, min_res = evaluate_specimens(specimens)
#     best = specimens[np.argmax(results)]
#     data["Generation"].append(epoch)
#     data["Max result"].append(max_res)
#     data["Min result"].append(min_res)
#     data["Start n"].append(best.start_n)
#     data["End n"].append(best.end_n)
#     data["Length"].append(best.length)
#     data["K"].append(best.k)
#     print("Epoch: {}\tMin result: {:.3f}\tMax result: {:.3f}\tStart N: {}\tEnd N:{}\tLength: {}\tK: {}".format(epoch,
#                                                                                                                min_res,
#                                                                                                                max_res,
#                                                                                                                best.start_n,
#                                                                                                                best.end_n,
#                                                                                                                best.length,
#                                                                                                                best.k))
#     specimens = reproduce(specimens, results, dna_range)
#
#   data = pd.DataFrame(data)
#   data.set_index("Generation", inplace=True)
#   print(data)
#   data.to_csv("exh.csv")