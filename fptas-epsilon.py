import numpy as np
from time import time
from fptas import random_test

import matplotlib.pyplot as plt

def running_time(epsilon):
  st = time()
  _ = random_test(epsilon=epsilon)
  et = time()
  return et - st

if __name__ == '__main__':
  iterations = 1000
  epsilons = np.arange(0, 1, 0.1)
  results = np.zeros(10)

  for index, epsilon in enumerate(epsilons):
    results[index] += running_time(epsilon)

  results /= iterations
  results *= 1000


  plt.figure()
  plt.plot(epsilons, results)
  plt.xlabel("Epsilon")
  plt.ylabel("Time performance (ms)")
  plt.xticks(epsilons)
  plt.title("FPATS  - epsilon performance")
  plt.show()
  print(epsilons)